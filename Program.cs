﻿using Sandbox.Game.EntityComponents;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VRage;
using VRage.Collections;
using VRage.Game;
using VRage.Game.Components;
using VRage.Game.GUI.TextPanel;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ModAPI.Ingame.Utilities;
using VRage.Game.ObjectBuilders.Definitions;
using VRageMath;

namespace IngameScript
{
    partial class Program : MyGridProgram
    {
        private List<IMyTextSurface> surfaces;
        private List<IMyTerminalBlock> storages;

        private const String DISPLAYS_SECTION = "displays";
        private const String CARGOTYPES_SECTION = "cargotypes";

        private void createDefaultConfig()
        {
            Me.CustomData = "[" + DISPLAYS_SECTION + "]\n" +
                "0.name=" + Me.CustomName + "\n" +
                "0.display=0\n" +
                "\n" +
                "[" + CARGOTYPES_SECTION + "]\n" +
                "enabled=\n" +
                "|CargoContainer\n" +
                "|Collector\n" +
                "|ShipDrill\n" +
                "|ShipConnector\n" +
                "|ShipGrinder\n";
        }

        private void findBlocksForType(String type, List<IMyTerminalBlock> resultList)
        {
            switch (type)
            {
                case "CargoContainer":
                    GridTerminalSystem.GetBlocksOfType<IMyCargoContainer>(resultList, b => b.IsSameConstructAs(Me));
                    break;
                case "Collector":
                    GridTerminalSystem.GetBlocksOfType<IMyCollector>(resultList, b => b.IsSameConstructAs(Me));
                    break;
                case "ShipDrill":
                    GridTerminalSystem.GetBlocksOfType<IMyShipDrill>(resultList, b => b.IsSameConstructAs(Me));
                    break;
                case "ShipConnector":
                    GridTerminalSystem.GetBlocksOfType<IMyShipConnector>(resultList, b => b.IsSameConstructAs(Me));
                    break;
                case "ShipGrinder":
                    GridTerminalSystem.GetBlocksOfType<IMyShipGrinder>(resultList, b => b.IsSameConstructAs(Me));
                    break;
            }
        }

        public Program()
        {
            this.Runtime.UpdateFrequency = UpdateFrequency.Update100;
            initBlocks();
        }

        private void initBlocks()
        {
            if (Me.CustomData.Length == 0)
            {
                createDefaultConfig();
            }

            var ini = new MyIni();
            MyIniParseResult result;
            if (!ini.TryParse(Me.CustomData, out result))
            {
                throw new Exception(result.ToString());
            }

            surfaces = new List<IMyTextSurface>();

            for (var index = 0; true; index++)
            {
                var nameKey = index + ".name";
                if (!ini.ContainsKey(DISPLAYS_SECTION, nameKey))
                {
                    break;
                }

                String displayKey;
                ini.Get(DISPLAYS_SECTION, nameKey).TryGetString(out displayKey);

                Echo("displayKey: " + displayKey);
                var surfaceProvider = GridTerminalSystem.GetBlockWithName(displayKey) as IMyTextSurfaceProvider;
                if (surfaceProvider == null)
                {
                    Echo("Could not find Display Block " + displayKey);
                    continue;
                }

                int displayIndex = 0;
                var displayIndexKey = index + ".display";
                if (ini.ContainsKey(DISPLAYS_SECTION, displayIndexKey))
                {
                    ini.Get(DISPLAYS_SECTION, displayIndexKey).TryGetInt32(out displayIndex);
                }

                var display = surfaceProvider.GetSurface(displayIndex);
                if (display == null)
                {
                    Echo("Could not access Display " + displayIndex + " on Display Block " + displayKey);
                    continue;
                }

                surfaces.Add(display);
            }

            var cargotypeKeys = new List<String>();
            ini.Get(CARGOTYPES_SECTION, "enabled").GetLines(cargotypeKeys);

            storages = new List<IMyTerminalBlock>();
            var tmpList = new List<IMyTerminalBlock>();
            foreach (var cargotypeKey in cargotypeKeys)
            {
                findBlocksForType(cargotypeKey, tmpList);
                storages.AddList(tmpList);
            }
        }

        void Main(string args, UpdateType updateSource)
        {
            if (updateSource == UpdateType.Terminal)
            {
                initBlocks();
            }
            if (updateSource != UpdateType.Update100)
            {
                return;
            }

            int current = 0;
            int max = 0;

            foreach (var storage in storages)
            {
                var storageInfo = getInventoryFilling(storage);
                current += storageInfo.Key;
                max += storageInfo.Value;
                var storagePercentage = (storageInfo.Value == 0) ? 0 : storageInfo.Key * 100.0 / storageInfo.Value;

                Echo(storage.CustomName + ": " + storagePercentage.ToString("0.0"));
            }

            double percentage = (max == 0) ? 0 : current * 100.0 / max;
            string volume = percentage.ToString("0.0");

            foreach (var surface in surfaces)
            {
                surface.WriteText(volume);
            }

            Echo("Current Inventory Volume: " + volume + "%");
        }
        private KeyValuePair<int, int> getInventoryFilling(IMyTerminalBlock storage)
        {
            int max = 0;
            int current = 0;
            for (var i = 0; i < storage.InventoryCount; i++)
            {
                var inventory = storage.GetInventory(i);
                max += (int)inventory.MaxVolume;
                current += (int)inventory.CurrentVolume;
            }

            return new KeyValuePair<int, int>(current, max);
        }
    }
}
